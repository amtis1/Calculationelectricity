

<html>
<head>
    <style>
        input[type=text] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            border: 2px solid grey;
            border-radius: 10px;
        }

        .form-container {
            width: 50%;
            margin: 0 auto;
        }

        table {
            width: 50%;
            border-collapse: collapse;
            margin: 0 auto;
        }

        th, td {
            border: 1px solid black;
            padding: 8px;
        }

        th {
            font-weight: bold;
        }

        .button {
            display: block;
            width: 200px;
            margin: 0 auto;
            padding: 10px;
            border: 1px solid blue;
            color: grey;
            text-align: center;
            border-radius: 10px;
            font-family: Arial, sans-serif;
        }

        .result-container {
            color: #191369;
            border: 1px solid #87CEFA;
            padding: 10px;
            margin: 20px auto;
            max-width: 50%;
            box-sizing: border-box;
            font-family: Arial, sans-serif;
            border-radius: 10px;
            font-weight: bold;
        }
    </style>
</head>
<body>
    <h2 style="text-align: center;">CALCULATE</h2>

    <div class="form-container">
        <form action="index.php" method="post">
            <label for="voltage">Voltage (V)</label>
            <input type="text" id="voltage" name="voltage" value="<?php if (isset($_POST['voltage'])) { echo $_POST['voltage']; } ?>">
            <br><br>
            <label for="current">Current (A)</label>
            <input type="text" id="current" name="current" value="<?php if (isset($_POST['current'])) { echo $_POST['current']; } ?>">
            <br><br>
            <label for="currentrate">Current Rate (sen/kWh)</label>
            <input type="text" id="currentrate" name="currentrate" value="<?php if (isset($_POST['currentrate'])) { echo $_POST['currentrate']; } ?>">
            <br><br>
            <input type="submit" name="submit_calculate" class="button" value="Calculate">
        </form>
    </div>
</body>
</html>
<?php
session_start();
$dbc = mysqli_connect("localhost", "root", "", "electricitybill");
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

function calculateEnergyAndTotal($power, $currentrate) {
    $result = [];
    for ($time = 1; $time <= 24; $time++) {
        $energy = $power * $time * 1000;
        $energyFormatted = number_format($energy / (1000 * 1000), 5);
        $total = $energyFormatted * ($currentrate / 100);
        $totalRounded = round($total, 2);

        $result[] = [
            'time' => $time,
            'energy' => $energyFormatted,
            'total' => $totalRounded
        ];
    }
    return $result;
}

if (isset($_POST['submit_calculate'])) {
    $voltage = $_POST['voltage'];
    $current = $_POST['current'];
    $currentrate = $_POST['currentrate'];

    $power = $voltage * $current;
    $rate = $currentrate / 100;

    $powerFormatted = number_format($power, 5, '.', '');
    $rateFormatted = number_format($rate, 3, '.', '');

    echo '<div class="result-container">';
    echo "<p>Power: $powerFormatted kw</p>";
    echo "<p>Rate: $rateFormatted RM</p>";
    echo '</div>';

    $calculationResult = calculateEnergyAndTotal($power, $currentrate);

    echo '<table>';
    echo '<tr>';
    echo '<th>#</th>';
    echo '<th>Hour</th>';
    echo '<th>Energy (kWh)</th>';
    echo '<th>Total</th>';
    echo '</tr>';

    foreach ($calculationResult as $result) {
        echo '<tr>';
        echo '<td><b>' . $result['time'] . '</b></td>';
        echo '<td>' . $result['time'] . '</td>';
        echo '<td>' . $result['energy'] . '</td>';
        echo '<td>' . $result['total'] . '</td>';
        echo '</tr>';
    }

    echo '</table>';
}
?>



















